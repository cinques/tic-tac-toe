$(document).ready(function () {
	var game_board = [[],[],[]];
	var turn = true; // первыми ходят крестики

	$('#board td').click(function (x) {
		var cell = $(this);

		var column = cell.index();
		var row = cell.parent().index();

		if (!cell.hasClass("cross") && !cell.hasClass("nought")) {
			game_board[row][column] = turn;
			cell.addClass(turn ? "cross" : "nought");
		}

		if (check_win(row, column))	{
			alert((turn ? "crosses" : "noughts") + " win!");
			clear_board();
		}
		
		turn = !turn; // меняем ход
	});

	function check_win(row, column) {
		return check_lines(column) || check_columns(row) || check_center();
	}

	function check_lines(column) {
		var win = true;
		for (var i = 1; i < 3; i++)
			if (game_board[i][column] != game_board[i-1][column]) 
				win = false;
		return win;
	}

	function check_columns(row) {
		var win = true;
		for (var j = 1; j < 3; j++)
			if (game_board[row][j] != game_board[row][j-1]) 
				win = false;
		return win;
	}

	function check_center() {
		if (game_board[0][0] == game_board[1][1] && game_board[1][1] == game_board[2][2] ||
			game_board[2][0] == game_board[1][1] && game_board[1][1] == game_board[0][2])
		{
			if (typeof game_board[1][1] != 'undefined')
				return true;
		}
				
		return false;
	}

	function clear_board() {
		game_board = [[],[],[]];
		$('.nought').removeClass('nought');
		$('.cross').removeClass('cross');
	}
});